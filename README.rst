Fluyt - the web-page curl
=========================

Fluyt acts as a stateful web browser. It doesn't display the rendered web-page,
only its `<body>` element. It is designed to work in a console-only environment.

About the name
--------------

Following the tradition of naming browsers after boats, this project is called
Fluyt. Quoting the `Fluyt Wikipedia`_ page:

A fluyt is a Dutch type of sailing vessel originally designed by the shipwrights
of Hoorn as a dedicated cargo vessel. Originating in the Dutch Republic in
the 16th century, the vessel was designed to facilitate transoceanic delivery
with the maximum of space and crew efficiency. Unlike rivals, it was not built
for conversion in wartime to a warship, so it was cheaper to build and carried
twice the cargo, and could be handled by a smaller crew

.. _Fluyt Wikipedia: https://en.wikipedia.org/wiki/Fluyt

.. image:: Wenceslas_Hollar_-_A_Flute_(State_2).jpg
   :align: center
      
In comparison, this projects provides a D-Bus service relying on a single
WebView, able to load on-demand a high volume of websites, mostly for scraping
purpose.

Installation
------------

The main dependency of Fluyt is WPEWebKit_. Keeping such a big project
up-to-date as a binary package is challenging. However the kind folks at Igalia_
maintain a Flatpak repository for it. Fluyt is packaged as a third-party Flatpak
app based upon their runtime. So installing Fluyt is as simple as:

::

  $ flatpak install --user https://base-art.net/fluyt.flatpakref

This setup is known to work even on old distributions such as Debian Stretch,
which ships flatpak 0.8.9. Flatpak apps can run fine on headless setups, a
full-blown desktop environment is not required.

Running
-------

To run Fluyt, invoke it through flatpak:

::

  $ flatpak run net.baseart.Fluyt//stable <URL> [<js-script-file> <response-type>]

If not argument is provided on the command-line, Fluyt will start a D-Bus service
that can be queried like this:

::

  $ dbus-send --print-reply --dest=net.baseart.Fluyt.Server /net/baseart/Fluyt/Object \
    net.baseart.Fluyt.Interface.LoadUri string:'<URL>'

The advantage of the D-Bus service is that the WebView will be created once only
and reused on-demand, so this user-case is more suitable and performant when
multiple websites need to be loaded in batch.

A simple Python3 client is also provided for convenience:

::

  $ pip3 install --user dbussy
  $ python3 fluyt-client.py <method-name> <URL> [<js-script-file> <response-type>]

Where `method-name` is either `LoadUri` or `LoadUriAndEvaluateScript`. An
example script can look like this, for instance:

::

  let result = {"title": document.title}
  result

Fluyt will return the value of the last evaluated expression from the JS script.
A common practice is to hence use `json` as response type and make sure your
script final statement evaluates to a JSON payload. JSON is currently the only
supported response-type.

.. _WPEWebKit: https://wpewebkit.org
.. _Igalia: https://igalia.com
