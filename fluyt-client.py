# Copyright 2019 Philippe Normand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pip3 install --user dbussy
import dbussy as dbus
from dbussy import DBUS
import asyncio
import sys
import os

def main(args):
    if not args:
        print("usage: %s <method-name> <URI> [extra params...]" % sys.argv[0])
        return 1

    method_name = args[0]
    uri = args[1]
    extra_params = args[2:]

    destination = "net.baseart.Fluyt.Server"
    object_path = "/net/baseart/Fluyt/Object"
    interface_name = "net.baseart.Fluyt.Interface"

    dbus.validate_bus_name(destination)
    dbus.validate_path(object_path)
    dbus.validate_interface(interface_name)
    dbus.validate_member(method_name)

    loop = asyncio.get_event_loop()
    conn = loop.run_until_complete(dbus.Connection.bus_get_async(DBUS.BUS_SESSION, private=False))
    message = dbus.Message.new_method_call(destination=destination, path=object_path,
                                           iface=interface_name, method=method_name)
    message.append_objects((dbus.BasicType(dbus.TYPE.STRING),), uri)
    fds = []
    for param in extra_params:
        if os.path.isfile(param):
            f = open(param)
            fds.append(f)
            value = f.fileno()
            value_type = dbus.TYPE.UNIX_FD
        else:
            value = param
            value_type = dbus.TYPE.STRING
        message.append_objects((dbus.BasicType(value_type),), value)

    reply = loop.run_until_complete(conn.send_await_reply(message))
    print(reply.all_objects[0])
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
