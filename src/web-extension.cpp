/* web-extension.cpp
 *
 * Copyright 2019 Philippe Normand
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <wpe/webkit-web-extension.h>
#include <wpe/webkitdom.h>

extern "C" {

typedef struct _AppData {
    WebKitWebPage* web_page;
    gchar* script;
    gchar* response_type;
} AppData;

static void
app_data_free(gpointer data, GClosure*)
{
    AppData* app_data = (AppData*)data;
    if (app_data->script)
        g_free(app_data->script);
    if (app_data->response_type)
        g_free(app_data->response_type);
    g_free(data);
}

static gboolean
dump_body(gpointer data)
{
    AppData* app_data = (AppData*)data;
    WebKitWebPage* web_page = app_data->web_page;
    WebKitDOMDocument* document;
    JSCContext* context = nullptr;
    g_autofree gchar* payload = nullptr;
    WebKitFrame* frame;
    g_autoptr(JSCValue) js_document = nullptr;
    g_autoptr(JSCValue) value = nullptr;
    g_autoptr(JSCValue) body_element = nullptr;

    document = webkit_web_page_get_dom_document(web_page);
    frame = webkit_web_page_get_main_frame(web_page);
    js_document = webkit_frame_get_js_value_for_dom_object(
        frame, WEBKIT_DOM_OBJECT(document));

    if (app_data->script) {
        context = jsc_value_get_context(js_document);
        value = jsc_context_evaluate(context, app_data->script, -1);
        JSCException* exception = jsc_context_get_exception(context);
        if (exception) {
            g_autofree gchar* report = jsc_exception_report(exception);
            g_autoptr(GVariant) parameters = g_variant_new_string(static_cast<const gchar*>(g_steal_pointer(&report)));
            jsc_context_clear_exception(context);
            WebKitUserMessage* message = webkit_user_message_new("error", static_cast<GVariant*>(g_steal_pointer(&parameters)));
            webkit_web_page_send_message_to_view(web_page, message, nullptr, nullptr, nullptr);
            return G_SOURCE_REMOVE;
        }
        jsc_context_clear_exception(context);

        if (g_str_equal(app_data->response_type, "json")) {
            payload = jsc_value_to_json(value, 2);
            if (!payload)
                payload = g_strdup("{}");
        } else
            payload = jsc_value_to_string(value);
        g_free(app_data->script);
        app_data->script = nullptr;
        g_free(app_data->response_type);
        app_data->response_type = nullptr;
    } else {
        value = jsc_value_object_get_property(js_document, "body");
        body_element = jsc_value_object_get_property(value, "innerHTML");
        payload = jsc_value_to_string(body_element);
    }

    g_autoptr(GVariant) parameters = g_variant_new_string(payload);
    WebKitUserMessage* message = webkit_user_message_new("payload", static_cast<GVariant*>(g_steal_pointer(&parameters)));
    webkit_web_page_send_message_to_view(web_page, message, nullptr, nullptr, nullptr);
    return G_SOURCE_REMOVE;
}

static void
document_loaded_callback(WebKitWebPage* web_page, gpointer user_data)
{
    WebKitDOMDocument* document;
    WebKitFrame* frame;
    g_autoptr(JSCValue) js_document = nullptr;
    JSCContext* context = nullptr;
    g_autoptr(JSCValue) func = nullptr;
    AppData* app_data = (AppData*)user_data;

    app_data->web_page = web_page;
    document = webkit_web_page_get_dom_document(web_page);
    frame = webkit_web_page_get_main_frame(web_page);
    js_document = webkit_frame_get_js_value_for_dom_object(
        frame, WEBKIT_DOM_OBJECT(document));
    context = jsc_value_get_context(js_document);
    const gchar* script = "window.scrollTo(0, 200);";
    g_autoptr(JSCValue) result = jsc_context_evaluate(context, script, strlen(script));
    jsc_context_clear_exception(context);

    g_timeout_add_seconds(1, dump_body, user_data);
}

static gboolean
user_message_callback(WebKitWebPage* web_page, WebKitUserMessage* message, gpointer user_data)
{
    if (g_str_equal(webkit_user_message_get_name(message), "load-parameters")) {
        AppData* app_data = (AppData*)user_data;
        GVariant* parameters = webkit_user_message_get_parameters(message);
        gchar* response_type;
        gchar* script;

        g_variant_get(parameters, "(&sss)", nullptr, &script, &response_type);
        if (app_data->script)
            g_free(app_data->script);
        app_data->script = script;
        if (app_data->response_type)
            g_free(app_data->response_type);
        app_data->response_type = response_type;
        return TRUE;
    }

    return FALSE;
}

static void
web_page_created_callback(WebKitWebExtension* extension,
    WebKitWebPage* web_page,
    gpointer user_data)
{
    g_signal_connect(web_page, "document-loaded",
        G_CALLBACK(document_loaded_callback), user_data);
    g_signal_connect(web_page, "user-message-received",
        G_CALLBACK(user_message_callback), user_data);
}

G_MODULE_EXPORT void
webkit_web_extension_initialize_with_user_data(WebKitWebExtension* extension,
    GVariant* user_data)
{
    AppData* app_data = g_new(AppData, 1);
    app_data->web_page = nullptr;
    app_data->script = nullptr;
    app_data->response_type = nullptr;

    g_signal_connect_data(extension, "page-created",
        G_CALLBACK(web_page_created_callback), app_data,
        app_data_free, G_CONNECT_AFTER);
}
}
