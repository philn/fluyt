/* headless-backend.cpp
 *
 * Copyright 2019 Philippe Normand
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <EGL/eglplatform.h>
#include <glib.h>
#include <wpe/wpe-egl.h>
#include <wpe/wpe.h>

extern "C" {

struct wpe_renderer_host_interface fluyt_renderer_host = {
    // create
    []() -> void* {
        return nullptr;
    },
    // destroy
    [](void* data) {
    },
    // create_client
    [](void* data) -> int {
        return 0;
    },
};

struct wpe_renderer_backend_egl_interface fluyt_renderer_backend_egl = {
    // create
    [](int host_fd) -> void* {
        return nullptr;
    },
    // destroy
    [](void* data) {
    },
    // get_native_display
    [](void* data) -> EGLNativeDisplayType {
        return 0;
    },
};

struct wpe_renderer_backend_egl_target_interface
    fluyt_renderer_backend_egl_target
    = {
          // create
          [](struct wpe_renderer_backend_egl_target* target,
              int host_fd) -> void* {
              return nullptr;
          },
          // destroy
          [](void* data) {
          },
          // initialize
          [](void* data, void* backend_data, uint32_t width, uint32_t height) {
          },
          // get_native_window
          [](void* data) -> EGLNativeWindowType {
              return 0;
          },
          // resize
          [](void* data, uint32_t width, uint32_t height) {
          },
          // frame_will_render
          [](void* data) {
          },
          // frame_rendered
          [](void* data) {
          },
      };

__attribute__((visibility(
    "default"))) struct wpe_loader_interface _wpe_loader_interface
    = {
          [](const char* object_name) -> void* {
              if (!strcmp(object_name, "_wpe_renderer_host_interface"))
                  return &fluyt_renderer_host;

              if (!strcmp(object_name, "_wpe_renderer_backend_egl_interface"))
                  return &fluyt_renderer_backend_egl;
              if (!strcmp(object_name, "_wpe_renderer_backend_egl_target_interface"))
                  return &fluyt_renderer_backend_egl_target;

              return nullptr;
          },
      };
}
