/* main.cpp
 *
 * Copyright 2019 Philippe Normand
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fluyt-config.h"

#include <glib-unix.h>
#include <glib.h>
#include <locale.h>
#include <stdlib.h>
#include <wpe/webkit.h>
#include <gio/gunixfdlist.h>

#if ENABLE_SHM
#include <wpe/fdo.h>
#include <wpe/unstable/fdo-shm.h>
#endif

static GDBusNodeInfo* introspection_data = NULL;

static const gchar introspection_xml[] = "<node>"
                                         "  <interface name='net.baseart.Fluyt.Interface'>"
                                         "    <method name='LoadUri'>"
                                         "      <arg type='s' name='uri' direction='in'/>"
                                         "      <arg type='s' name='response' direction='out'/>"
                                         "    </method>"
                                         "    <method name='LoadUriAndEvaluateScript'>"
                                         "      <arg type='s' name='uri' direction='in'/>"
                                         "      <arg type='h' name='script' direction='in'/>"
                                         "      <arg type='s' name='response_type' direction='in'/>"
                                         "      <arg type='s' name='response' direction='out'/>"
                                         "    </method>"
                                         "  </interface>"
                                         "</node>";
typedef struct _AppData {
    GMainLoop* loop;
    gboolean is_server;
    WebKitWebView* web_view;
#if ENABLE_SHM
    struct wpe_view_backend_exportable_fdo* exportable;
#endif
    GDBusMethodInvocation* current_invocation;
    gint exit_status;
    gchar* current_uri;
} AppData;

void app_data_free(gpointer user_data)
{
    AppData* app_data = (AppData*)user_data;
    g_main_loop_unref(app_data->loop);
    g_object_unref(app_data->web_view);
    g_free(app_data);
}

static void
on_web_process_terminated(WebKitWebView*, WebKitWebProcessTerminationReason, gpointer user_data)
{
    AppData* app_data = (AppData*)user_data;
    g_printerr("WebProcess terminated\n");
    app_data->exit_status = EXIT_FAILURE;
    g_main_loop_quit(app_data->loop);
}

#if ENABLE_SHM
struct wpe_view_backend_exportable_fdo_client s_exportableClient = {
    nullptr,
    nullptr,
    // export_shm_buffer
    [](void* data, struct wpe_fdo_shm_exported_buffer* buffer) {
      AppData* app_data = (AppData*)data;
      wpe_view_backend_exportable_fdo_dispatch_release_shm_exported_buffer(app_data->exportable, buffer);
    },
    nullptr,
    nullptr,
};
#endif

static gboolean
on_user_message_received(WebKitWebView*, WebKitUserMessage* message, gpointer user_data)
{
    const gchar* name = webkit_user_message_get_name(message);
    AppData* data = (AppData*)user_data;
    GVariant* parameters = webkit_user_message_get_parameters(message);

    if (g_str_equal(name, "payload")) {
        const gchar* response = g_variant_get_string(parameters, nullptr);
        if (data->is_server) {
            if (data->current_invocation)
                g_dbus_method_invocation_return_value(data->current_invocation, g_variant_new("(s)", response));
            data->current_invocation = NULL;
        } else {
            g_print("%s\n", response);
            g_main_loop_quit(data->loop);
        }
        return TRUE;
    } else if (g_str_equal(name, "error")) {
      const gchar* response = g_variant_get_string(parameters, nullptr);
      if (data->is_server) {
        if (data->current_invocation)
          g_dbus_method_invocation_return_error(data->current_invocation, G_IO_ERROR,
                                                G_IO_ERROR_FAILED_HANDLED, response);
        data->current_invocation = NULL;
      } else {
        g_print("%s\n", response);
        data->exit_status = EXIT_FAILURE;
        g_main_loop_quit(data->loop);
      }
      return TRUE;
    }
    return FALSE;
}

static gpointer
initialize_web_view(AppData* app_data)
{
    WebKitWebViewBackend* view_backend = NULL;
#if ENABLE_SHM
    wpe_loader_init("libWPEBackend-fdo-1.0.so");
    wpe_fdo_initialize_shm();
    app_data->exportable = wpe_view_backend_exportable_fdo_create(&s_exportableClient, app_data, 800, 600);
    auto* wpeViewBackend = wpe_view_backend_exportable_fdo_get_view_backend(app_data->exportable);
    view_backend = webkit_web_view_backend_new(wpeViewBackend, nullptr, nullptr);
#else
    static struct wpe_view_backend_interface s_backend_interface = {
        // create
        [](void*, struct wpe_view_backend*) -> void* { return nullptr; },
        // destroy
        [](void*) {},
        // initialize
        [](void*) {},
        // get_renderer_host_fd
        [](void*) -> int { return -1; },
        // padding
        nullptr, nullptr, nullptr, nullptr
    };
    wpe_loader_init("libWPEBackend-fluyt.so");
    view_backend = webkit_web_view_backend_new(
        wpe_view_backend_create_with_backend_interface(&s_backend_interface,
            nullptr),
        NULL, NULL);
#endif

    g_autoptr(WebKitWebContext) context = webkit_web_context_new_ephemeral();
    webkit_web_context_set_web_extensions_directory(context, LIBDIR);

    g_autoptr(WebKitWebView) web_view = webkit_web_view_new_with_context(view_backend, context);

    g_signal_connect(web_view, "web-process-terminated", G_CALLBACK(on_web_process_terminated), app_data);
    g_signal_connect(web_view, "user-message-received", G_CALLBACK(on_user_message_received), app_data);

    WebKitSettings* settings = webkit_web_view_get_settings(web_view);
    webkit_settings_set_enable_media(settings, FALSE);
    webkit_settings_set_enable_page_cache(settings, FALSE);
    webkit_settings_set_enable_offline_web_application_cache(settings, FALSE);
    webkit_settings_set_enable_webgl(settings, FALSE);
    return g_steal_pointer(&web_view);
}

static void
on_message_sent(GObject* object, GAsyncResult* result, gpointer user_data)
{
    AppData* app_data = (AppData*)user_data;
    webkit_web_view_load_uri(app_data->web_view, app_data->current_uri);
}

static gchar *
read_all_from_fd(gint fd, gsize* out_len, GError** error)
{
    GString *str;
    gchar buf[64];
    gssize num_read;

    str = g_string_new(NULL);
    do {
        int errsv;
        num_read = read(fd, buf, sizeof(buf));
        errsv = errno;
        if (num_read == -1) {
            if (errsv == EAGAIN || errsv == EWOULDBLOCK)
                continue;
            g_set_error(error, G_IO_ERROR,
                        g_io_error_from_errno (errsv),
                        "Failed reading %d bytes into offset %d: %s",
                        (gint) sizeof(buf),
                        (gint) str->len,
                        g_strerror(errsv));
            goto error;
        } else if (num_read > 0) {
            g_string_append_len(str, buf, num_read);
        } else if (num_read == 0) {
            break;
        }
    } while(TRUE);

    if (out_len != NULL)
        *out_len = str->len;
    return g_string_free(str, FALSE);

 error:
    if (out_len != NULL)
        *out_len = 0;
    g_string_free(str, TRUE);
    return NULL;
}

static void
handle_method_call(GDBusConnection* connection,
    const gchar* sender,
    const gchar* object_path,
    const gchar* interface_name,
    const gchar* method_name,
    GVariant* parameters,
    GDBusMethodInvocation* invocation,
    gpointer user_data)
{
    AppData* app_data = (AppData*)user_data;
    if (g_str_has_prefix(method_name, "LoadUri")) {
        const gchar* uri;

        app_data->current_invocation = invocation;

        if (g_strcmp0(method_name, "LoadUriAndEvaluateScript") == 0) {
            GDBusMessage * dbus_message = g_dbus_method_invocation_get_message(invocation);
            GUnixFDList* fd_list = g_dbus_message_get_unix_fd_list(dbus_message);
            gint script_fd = g_unix_fd_list_get(fd_list, 0, nullptr);
            const gchar* response_type;

            // Read the fd contents here. Dunno why the FD cannot be relayed to the web-extension...
            gsize len = 0;
            g_autofree gchar* script = read_all_from_fd(script_fd, &len, nullptr);
            close(script_fd);

            if (app_data->current_uri)
                g_free(app_data->current_uri);

            g_variant_get(parameters, "(sh&s)", &app_data->current_uri, nullptr, &response_type);
            g_autoptr(GVariant) new_parameters = g_variant_new("(sss)", app_data->current_uri, static_cast<gchar*>(g_steal_pointer(&script)), response_type);

            WebKitUserMessage* message = webkit_user_message_new("load-parameters", static_cast<GVariant*>(g_steal_pointer(&new_parameters)));
            webkit_web_view_send_message_to_page(app_data->web_view, message, nullptr,
                                                 on_message_sent, app_data);
        } else {
            g_variant_get(parameters, "(&s)", &uri);
            webkit_web_view_load_uri(app_data->web_view, uri);
        }
    }
}

static const GDBusInterfaceVTable interface_vtable = {
    handle_method_call,
    NULL,
    NULL,
};

static void
on_bus_acquired(GDBusConnection* connection,
    const gchar* name,
    gpointer user_data)
{
    guint registration_id;

    registration_id = g_dbus_connection_register_object(
        connection, "/net/baseart/Fluyt/Object",
        introspection_data->interfaces[0], &interface_vtable, user_data,
        app_data_free, NULL);
    g_assert(registration_id > 0);
}

static void
on_name_acquired(GDBusConnection* connection,
    const gchar* name,
    gpointer user_data)
{
}

static void
on_name_lost(GDBusConnection* connection,
    const gchar* name,
    gpointer user_data)
{
    exit(1);
}

gint main(gint argc, gchar* argv[])
{
    g_autoptr(GOptionContext) context = NULL;
    g_autoptr(GError) error = NULL;
    g_autoptr(GMainLoop) loop = NULL;
    gboolean version = FALSE;
    GStrv arguments = NULL;
    GOptionEntry main_entries[] = {
        { "version", 0, 0, G_OPTION_ARG_NONE, &version, "Show program version" },
        { G_OPTION_REMAINING, '\0', 0, G_OPTION_ARG_FILENAME_ARRAY, &arguments, "",
            "[URL]" },
        { NULL }
    };
    guint owner_id = 0;
    AppData* app_data = NULL;
    gboolean is_server = FALSE;

    setlocale(LC_ALL, "");

    context = g_option_context_new("- Fluyt!");
    g_option_context_add_main_entries(context, main_entries, NULL);

    if (!g_option_context_parse(context, &argc, &argv, &error)) {
        g_printerr("%s\n", error->message);
        return EXIT_FAILURE;
    }

    if (version) {
        g_autofree gchar* extra = nullptr;
#if ENABLE_SHM
        extra = g_strdup_printf(" and WPEBackend-FDO %u.%u.%u for SHM/Headless support", wpe_fdo_get_major_version(), wpe_fdo_get_minor_version(), wpe_fdo_get_micro_version());
#else
        extra = g_strdup("");
#endif
        g_print("Fluyt %s - Powered by WPEWebKit %d.%d.%d%s\n", PACKAGE_VERSION, webkit_get_major_version(),
                webkit_get_minor_version(), webkit_get_micro_version(), extra);
        return EXIT_SUCCESS;
    }

    is_server = arguments == NULL;
    loop = g_main_loop_new(NULL, TRUE);
    app_data = g_new(AppData, 1);
    app_data->is_server = is_server;
    app_data->loop = (GMainLoop*)g_main_loop_ref(loop);
    app_data->exit_status = EXIT_SUCCESS;
    app_data->web_view = (WebKitWebView*)g_object_ref(initialize_web_view(app_data));

    if (app_data->is_server) {
        introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);
        g_assert(introspection_data != NULL);

        owner_id = g_bus_own_name(G_BUS_TYPE_SESSION, "net.baseart.Fluyt.Server",
            G_BUS_NAME_OWNER_FLAGS_NONE, on_bus_acquired,
            on_name_acquired, on_name_lost, app_data,
            app_data_free);
    } else {
        const gchar* uri = arguments[0];
        if (g_strv_length(arguments) == 3) {
            g_autoptr(GFile) script = g_file_new_for_path(arguments[1]);
            g_autoptr(GFileInputStream) input_stream = g_file_read(script, nullptr, nullptr);
            g_autoptr(GFileInfo) info = g_file_input_stream_query_info(G_FILE_INPUT_STREAM(input_stream), G_FILE_ATTRIBUTE_STANDARD_SIZE, nullptr, nullptr);
            goffset size = g_file_info_get_size(info);
            g_autofree gchar* buffer = (gchar*)g_malloc(size);
            g_input_stream_read_all(G_INPUT_STREAM(input_stream), buffer, size, nullptr, nullptr, nullptr);

            app_data->current_uri = g_strdup(uri);
            g_autoptr(GVariant) parameters = g_variant_new("(sss)", uri, buffer, arguments[2]);
            WebKitUserMessage* message = webkit_user_message_new("load-parameters", static_cast<GVariant*>(g_steal_pointer(&parameters)));
            webkit_web_view_send_message_to_page(app_data->web_view, message, nullptr,
                                                 on_message_sent, app_data);
        } else {
          webkit_web_view_load_uri(app_data->web_view, uri);
        }
        g_strfreev(arguments);
    }

    g_main_loop_run(loop);
    if (is_server) {
        if (owner_id)
            g_bus_unown_name(owner_id);
        g_dbus_node_info_unref(introspection_data);
    }
    return app_data->exit_status;
}
